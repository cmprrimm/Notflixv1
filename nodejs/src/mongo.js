//Object data modelling library for mongo
const mongoose = require('mongoose');
var request = require('request');

var url = 'http://192.168.56.10:2375';
var amqp = require('amqplib/callback_api');

//Mongo db client library
//const MongoClient  = require('mongodb');

//Express web service library
const express = require('express')

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

//connection string listing the mongo servers. This is an alternative to using a load balancer. THIS SHOULD BE DISCUSSED IN YOUR ASSIGNMENT.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/notflixDB?replicaSet=cfgrs';

var os = require("os");
var myhostname = os.hostname();

let nodes = [];
var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);


//			if (data.ContainersRunning < data.Containers && nodes.hostname != jMessage.myhostname){
//				var create = {
  //  uri: url + "/v1.40/containers/create",
//	method: 'POST',
   //deploy an alpine container that runs echo hello world
//	json: {"Image": "sysadmin_node1", "Cmd": ["pm2-runtime", "mongo.js", "--watch" ]}
//				}
//			}
//};

setInterval(function(){
amqp.connect('amqp://test:test@192.168.56.10', function(error0, connection) {

if (error0) {
        throw error0;
      }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';
              var msg = JSON.stringify({myhostname, nodeID});

	      

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });
              channel.publish(exchange, '', Buffer.from(msg));

            });
});

}, 10000);


//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var movieSchema = new Schema({
  _id: Number,
  title: String,
  price: Number,
  genre: String,
  release: Date
});

var movieModel = mongoose.model('Movie', movieSchema, 'movie');


app.get('/', (req, res) => {
  movieModel.find({},'title price genre release', (err, movie) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(movie))
  }) 
})

app.post('/',  (req, res) => {
  var awesome_instance = new movieModel(req.body);
  awesome_instance.save(function (err) {
  if (err) res.send('Error');
    res.send(JSON.stringify(req.body))
  });
})

app.put('/',  (req, res) => {
  res.send('Got a PUT request at /')
})

app.delete('/',  (req, res) => {
  res.send('Got a DELETE request at /')
})

//bind the express web service to the port specified
app.listen(port, () => {
 console.log(`Express Application listening at port ` + port)
})


//create a number of subscribers to connect to publishers
    amqp.connect('amqp://test:test@192.168.56.10', function(error0, connection) {
      if (error0) {
              throw error0;
	}
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
                      
              var exchange = 'logs';

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });

              channel.assertQueue('', {
                        exclusive: true
                      }, function(error2, q) {
                                if (error2) {
                                            throw error2;
                                          }
                                console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                                channel.bindQueue(q.queue, exchange, '');

                                channel.consume(q.queue, function(msg) {
                                        jsonMessage = JSON.parse(msg.content);
						 nodes.some(node => node.hostname === jsonMessage.myhostname) ? (nodes.find(e => e.hostname  ===  jsonMessage.myhostname)).msgTimer = Date.now() :  nodes.push({"hostname": jsonMessage.myhostname, nodestat : "alive" , "msgTimer" : Date.now()  , "nodeID" : jsonMessage.nodeID, priority : 0 });
                                          }, {

                                                      noAck: true
   });
                              });
            });
    });


function createContainer() {

        var create = {
        uri: url + "/v1.41/containers/create",
      method: 'POST',
   //deploy an alpine container that runs echo hello world
    json: {"Image": "sysadmin_node1", "Cmd":  [ "pm2-runtime mongo.js --watch"]
}
};
request(create, function (error, response, createBody) {
        if(!error){
                console.log("Created " + JSON.stringify(createBody));
     

        var start = {
                uri : url + "/v1.41/containers/" + createBody.Id+ "/start",
                method: "POST",
                json: {}
        };

request(start, function (error, response, startBody) {
        if (!error) {
                console.log("Container Up");
    var wait = {
                                uri: url + "/v1.40/containers/" + createBody.Id + "/wait",
                    method: 'POST',
                            json: {}
                        };


                            request(wait, function (error, response, waitBody ) {
                                if (!error) {
                                        console.log("run wait complete, container will have started");

                        //send a simple get request for stdout from the container
                        request.get({
                            url: url + "/v1.40/containers/" + createBody.Id + "/logs?stdout=1",
                            }, (err, res, data) => {
                                    if (err) {
                                        console.log('Error:', err);
                                    } else if (res.statusCode !== 200) {
                                        console.log('Status:', res.statusCode);
                                    } else{
                                        //need to parse the json response to access
                                        console.log("Container stdout = " + data);
                                    }
                                });
                        }
                        });

}
});
}
});
};

//leader election.
setInterval(function(){
var timeNow = Date.now();
var length = nodes.length;
nodes.sort(function(a, b){return a.nodeID - b.nodeID});
//let index = nodes.indexOf();
var maxNodeID = nodes[nodes.length-1];
nodes[nodes.length - 1].priority = 1;
//console.log(nodes);
jMessage = jsonMessage;
console.log(nodes);
for (var q =0; q < nodes.length; q++) {
	if(timeNow - nodes[q].msgTimer  > 11*1000) {
		createContainer();
	    nodes.splice(q, 1);
	}
}
//let remove = nodes.find(f => f != jMessage);	
//nodes.splice(remove);
if (maxNodeID != jMessage){
	nodes.pop();
}
}, 11000);
